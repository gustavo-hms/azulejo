module Desktop exposing
    ( Desktop
    , Direction(..)
    , Split(..)
    , insert
    , layout
    )

import KWin
import Tree


type Direction
    = Horizontal
    | Vertical


type alias Ratio =
    Float


type Split
    = Split Direction Ratio


type alias Desktop =
    { id : Int
    , width : Int
    , height : Int
    , clients : Maybe (Tree.Zipper Split KWin.Client)
    }


insert : KWin.Client -> Desktop -> Desktop
insert client desktop =
    let
        addLeaf leaf zipper =
            case Tree.goUp zipper of
                Nothing ->
                    zipper
                        |> Tree.subtree
                        |> insertLeft leaf
                        |> Tree.zip
                        |> Tree.goToLeftChild

                Just upperSubtree ->
                    if Tree.isLeftChild zipper then
                        upperSubtree
                            |> Tree.modifySubtree (insertLeft leaf)
                            |> Tree.goToLeftChild

                    else
                        upperSubtree
                            |> Tree.modifySubtree (insertRight leaf)
                            |> Tree.goToRightChild

        clients =
            case desktop.clients of
                Nothing ->
                    (Just << Tree.zip << Tree.Leaf) client

                Just zipper ->
                    addLeaf client zipper
    in
    { desktop | clients = clients }


insertLeft : a -> Tree.Tree Split a -> Tree.Tree Split a
insertLeft client tree =
    case tree of
        (Tree.Leaf _) as leaf ->
            Tree.Node (Split Vertical 0.5) (Tree.Leaf client) leaf

        Tree.Node split _ _ ->
            Tree.Node split (Tree.Leaf client) (clockwise tree)


insertRight : a -> Tree.Tree Split a -> Tree.Tree Split a
insertRight client tree =
    case tree of
        (Tree.Leaf _) as leaf ->
            Tree.Node (Split Vertical 0.5) leaf (Tree.Leaf client)

        Tree.Node split _ _ ->
            Tree.Node split (counterclockwise tree) (Tree.Leaf client)


clockwise : Tree.Tree Split a -> Tree.Tree Split a
clockwise tree =
    case tree of
        Tree.Leaf _ ->
            tree

        Tree.Node (Split direction ratio) left right ->
            if direction == Horizontal then
                Tree.Node
                    (Split Vertical ratio)
                    (clockwise right)
                    (clockwise left)

            else
                Tree.Node
                    (Split Horizontal ratio)
                    (clockwise left)
                    (clockwise right)


counterclockwise : Tree.Tree Split a -> Tree.Tree Split a
counterclockwise tree =
    case tree of
        Tree.Leaf _ ->
            tree

        Tree.Node (Split direction ratio) left right ->
            if direction == Horizontal then
                Tree.Node
                    (Split Vertical ratio)
                    (counterclockwise left)
                    (counterclockwise right)

            else
                Tree.Node
                    (Split Horizontal ratio)
                    (counterclockwise right)
                    (counterclockwise left)


splitHorizontally : Float -> KWin.Geometry -> ( KWin.Geometry, KWin.Geometry )
splitHorizontally ratio geometry =
    let
        topHeight =
            floor <| ratio * toFloat geometry.height

        top =
            { x = geometry.x
            , y = geometry.y
            , width = geometry.width
            , height = topHeight
            }

        bottom =
            { x = geometry.x
            , y = geometry.y + topHeight
            , width = geometry.width
            , height = geometry.height - topHeight
            }
    in
    ( top, bottom )


splitVertically : Float -> KWin.Geometry -> ( KWin.Geometry, KWin.Geometry )
splitVertically ratio geometry =
    let
        leftWidth =
            floor <| ratio * toFloat geometry.width

        left =
            { x = geometry.x
            , y = geometry.y
            , width = leftWidth
            , height = geometry.height
            }

        right =
            { x = geometry.x + leftWidth
            , y = geometry.y
            , width = geometry.width - leftWidth
            , height = geometry.height
            }
    in
    ( left, right )


splitGeometry : Split -> KWin.Geometry -> ( KWin.Geometry, KWin.Geometry )
splitGeometry (Split direction ratio) geometry =
    case direction of
        Horizontal ->
            splitHorizontally ratio geometry

        Vertical ->
            splitVertically ratio geometry


geometries : KWin.Geometry -> Tree.Tree Split KWin.Client -> List ( KWin.WindowId, KWin.Geometry )
geometries geometry tree =
    case tree of
        Tree.Leaf client ->
            [ ( client.windowId, geometry ) ]

        Tree.Node split left right ->
            let
                ( leftGeometry, rightGeometry ) =
                    splitGeometry split geometry
            in
            geometries leftGeometry left ++ geometries rightGeometry right


layout : Desktop -> Cmd msg
layout desktop =
    let
        geometry =
            { x = 0, y = 0, width = desktop.width, height = desktop.height }
    in
    desktop.clients
        |> Maybe.map Tree.unzip
        |> Maybe.andThen (Tree.filter (not << .minimized))
        |> Maybe.map (geometries geometry)
        |> Maybe.map (List.map KWin.resize)
        |> Maybe.withDefault []
        |> Cmd.batch
