// KDE scripting doesn't provide a setTimeout function, required by Elm
function setTimeout(fn, delay) {
	var timer = new QTimer();
	timer.singleShot = true;
	timer.timeout.connect(fn);
	timer.start(delay);
}

var azulejo = Elm.Main.init();

workspace.clientAdded.connect(function(client) {
	azulejo.ports.clientAdded.send(clientInfo(client));
});

azulejo.ports.resize.subscribe(resize);

function clientInfo(client) {
	return {
		windowId: client.windowId,
		transient: client.transient,
		specialWindow: client.specialWindow,
		desktop: client.desktop,
		minimized: client.minimized,
		geometry: client.geometry
	};
}

function resize(args) {
	var windowId = args[0];
	var geometry = args[1];
	var client = workspace.getClient(windowId);
	// TODO treat null client
	client.geometry = geometry;
}
