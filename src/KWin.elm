port module KWin exposing
    ( Client
    , Geometry
    , Script
    , WindowId
    , clientAdded
    , resize
    , script
    )

import Platform.Cmd


type alias Script flags model msg =
    { init : flags -> ( model, Cmd msg )
    , update : msg -> model -> model
    , layout : model -> Cmd msg
    , subscriptions : model -> Sub msg
    }


script : Script flags model msg -> Program flags model msg
script kwinScript =
    let
        update msg model =
            let
                newModel =
                    kwinScript.update msg model
            in
            ( newModel, kwinScript.layout newModel )
    in
    Platform.worker
        { init = kwinScript.init
        , update = update
        , subscriptions = kwinScript.subscriptions
        }


type alias WindowId =
    Int


type alias Client =
    { windowId : WindowId
    , transient : Bool
    , specialWindow : Bool
    , desktop : Int
    , minimized : Bool
    , geometry : Geometry
    }


type alias Geometry =
    { x : Int
    , y : Int
    , width : Int
    , height : Int
    }


port clientAdded : (Client -> msg) -> Sub msg


port resize : ( WindowId, Geometry ) -> Cmd msg
