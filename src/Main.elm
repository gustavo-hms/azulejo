module Main exposing (Model, Msg(..), clientAdded, main, update)

import Desktop
import Json.Decode
import KWin
import Platform.Cmd



-- main : Program flags Model Msg


main =
    KWin.script
        { init = init
        , update = update
        , layout = Desktop.layout
        , subscriptions = clientAdded
        }



-- Model


type alias Model =
    Desktop.Desktop


init : () -> ( Model, Cmd Msg )
init _ =
    let
        initialModel =
            { id = 0, width = 1920, height = 1080, clients = Nothing }
    in
    ( initialModel, Cmd.none )


type Msg
    = NewClient KWin.Client



-- Update


update : Msg -> Model -> Model
update msg model =
    case msg of
        NewClient client ->
            Desktop.insert client model



-- Subscriptions


clientAdded : Model -> Sub Msg
clientAdded model =
    KWin.clientAdded NewClient
