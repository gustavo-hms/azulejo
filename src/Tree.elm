module Tree exposing
    ( Tree(..)
    , Zipper
    , filter
    , getLeaf
    , getNode
    , goToLeftChild
    , goToRightChild
    , goToRoot
    , goUp
    , isLeftChild
    , isRightChild
    , modifySubtree
    , subtree
    , unzip
    , zip
    )

import List exposing (isEmpty)
import Maybe exposing (andThen)


type Tree node leaf
    = Leaf leaf
    | Node node (Tree node leaf) (Tree node leaf)


type Crumb node leaf
    = LeftCrumb node (Tree node leaf)
    | RightCrumb node (Tree node leaf)


type alias Breadcrumbs node leaf =
    List (Crumb node leaf)


type alias Zipper node leaf =
    ( Tree node leaf, Breadcrumbs node leaf )


getNode : Zipper node leaf -> Maybe node
getNode ( tree, _ ) =
    case tree of
        Node x _ _ ->
            Just x

        _ ->
            Nothing


getLeaf : Zipper node leaf -> Maybe leaf
getLeaf ( tree, _ ) =
    case tree of
        Leaf x ->
            Just x

        _ ->
            Nothing


zip : Tree node leaf -> Zipper node leaf
zip tree =
    ( tree, [] )


unzip : Zipper node leaf -> Tree node leaf
unzip =
    goToRoot >> subtree


subtree : Zipper node leaf -> Tree node leaf
subtree ( tree, _ ) =
    tree


isLeftChild : Zipper node leaf -> Bool
isLeftChild ( _, breadcrumbs ) =
    case breadcrumbs of
        (LeftCrumb _ _) :: _ ->
            True

        _ ->
            False


isRightChild : Zipper node leaf -> Bool
isRightChild ( _, breadcrumbs ) =
    case breadcrumbs of
        (RightCrumb _ _) :: _ ->
            True

        _ ->
            False


goToLeftChild : Zipper node leaf -> Maybe (Zipper node leaf)
goToLeftChild ( tree, breadcrumbs ) =
    case tree of
        Node element leftTree rightTree ->
            Just ( leftTree, LeftCrumb element rightTree :: breadcrumbs )

        Leaf element ->
            Nothing


goToRightChild : Zipper node leaf -> Maybe (Zipper node leaf)
goToRightChild ( tree, breadcrumbs ) =
    case tree of
        Node element leftTree rightTree ->
            Just ( rightTree, RightCrumb element leftTree :: breadcrumbs )

        Leaf element ->
            Nothing


goUp : Zipper node leaf -> Maybe (Zipper node leaf)
goUp ( tree, breadcrumbs ) =
    case breadcrumbs of
        [] ->
            Nothing

        (LeftCrumb element rightTree) :: moreCrumbs ->
            Just ( Node element tree rightTree, moreCrumbs )

        (RightCrumb element leftTree) :: moreCrumbs ->
            Just ( Node element leftTree tree, moreCrumbs )


goToRoot : Zipper node leaf -> Zipper node leaf
goToRoot (( _, breadcrumbs ) as zipper) =
    if isEmpty breadcrumbs then
        zipper

    else
        Maybe.map goToRoot (goUp zipper)
            |> Maybe.withDefault zipper


merge : (a -> a -> a) -> Maybe a -> Maybe a -> Maybe a
merge f maybeA maybeB =
    case maybeA of
        Nothing ->
            maybeB

        Just a ->
            case maybeB of
                Nothing ->
                    maybeA

                Just b ->
                    Just <| f a b


filter : (leaf -> Bool) -> Tree node leaf -> Maybe (Tree node leaf)
filter predicate tree =
    case tree of
        Leaf leaf ->
            if predicate leaf then
                Just tree

            else
                Nothing

        Node node leftTree rightTree ->
            let
                leftBranch =
                    filter predicate leftTree

                rightBranch =
                    filter predicate rightTree
            in
            merge (Node node) leftBranch rightBranch


modifySubtree : (Tree node leaf -> Tree node leaf) -> Zipper node leaf -> Zipper node leaf
modifySubtree f ( tree, breadcrumbs ) =
    ( f tree, breadcrumbs )
