all: build/azulejo.kwinscript

build/main.js: src/*.elm
	elm make src/Main.elm --output build/main.js

build/script/contents/code/azulejo.js: src/native.js build/main.js
	mkdir -p build/script/contents/code
	cat build/main.js > build/script/contents/code/azulejo.js
	cat src/native.js >> build/script/contents/code/azulejo.js

build/azulejo.kwinscript: build/script/contents/code/azulejo.js
	cp metadata/metadata.desktop build/script/metadata.desktop
	cd build/script; zip -r ../azulejo.kwinscript *
