module TestDesktop exposing (suite)

import Desktop exposing (..)
import Expect
import KWin
import Maybe exposing (andThen, withDefault)
import Test exposing (..)
import Tree exposing (..)



-- Helper functions and data to build zippers easily


type alias ZipperInfo a b =
    -- Information to build a zipper:
    -- * tree: the subtree
    -- * focused: whether this subtree has a focused node
    -- * path: a list of goToLeftChild and goToRightChild functions that,
    --   applied to the zipper, reach the focused node.
    { tree : Tree a b
    , focused : Bool
    , path : List (Zipper a b -> Maybe (Zipper a b))
    }


zipper : ZipperInfo a b -> Zipper a b
zipper info =
    let
        initialZipper =
            zip info.tree
    in
    List.foldl andThen (Just initialZipper) info.path
        |> withDefault initialZipper


o : a -> ZipperInfo a b -> ZipperInfo a b -> ZipperInfo a b
o node leftInfo rightInfo =
    -- Define a Split node
    let
        tree =
            Node node leftInfo.tree rightInfo.tree
    in
    if leftInfo.focused then
        { tree = tree
        , focused = True
        , path = goToLeftChild :: leftInfo.path
        }

    else if rightInfo.focused then
        { tree = tree
        , focused = True
        , path = goToRightChild :: rightInfo.path
        }

    else
        { tree = tree
        , focused = False
        , path = []
        }


oo : b -> ZipperInfo a b
oo leaf =
    -- Define a KWin.Client node
    { tree = Leaf leaf
    , focused = False
    , path = []
    }



-- The x and xx functions mark a node the zipper should focus on


x : a -> ZipperInfo a b -> ZipperInfo a b -> ZipperInfo a b
x node leftInfo rightInfo =
    -- Define a focused Split node
    { tree = Node node leftInfo.tree rightInfo.tree
    , focused = True
    , path = []
    }


xx : b -> ZipperInfo a b
xx leaf =
    -- Define a focused KWin.Client node
    { tree = Leaf leaf
    , focused = True
    , path = []
    }



-- Scenarios


newClient : Int -> KWin.Client
newClient id =
    { windowId = id
    , transient = False
    , specialWindow = False
    , desktop = 1
    , minimized = False
    , geometry =
        { x = 100
        , y = 150
        , width = 800
        , height = 600
        }
    }


firstTree : Zipper Split KWin.Client
firstTree =
    zipper <|
        o (Split Vertical 0.5)
            (oo <| newClient 1)
            (o (Split Horizontal 0.4)
                (xx <| newClient 2)
                (oo <| newClient 3)
            )


secondTree : Zipper Split KWin.Client
secondTree =
    zipper <|
        o (Split Vertical 0.5)
            (oo <| newClient 1)
            (o (Split Horizontal 0.4)
                (xx <| newClient 4)
                (o (Split Vertical 0.4)
                    (oo <| newClient 3)
                    (oo <| newClient 2)
                )
            )


thirdTree : Zipper Split KWin.Client
thirdTree =
    zipper <|
        o (Split Vertical 0.5)
            (oo <| newClient 1)
            (o (Split Horizontal 0.4)
                (xx <| newClient 5)
                (o (Split Vertical 0.4)
                    (o (Split Horizontal 0.4)
                        (oo <| newClient 3)
                        (oo <| newClient 2)
                    )
                    (oo <| newClient 4)
                )
            )


emptyDesktop =
    { id = 1
    , width = 1000
    , height = 1000
    , clients = Nothing
    }


desktop clients =
    { emptyDesktop | clients = Just clients }


suite : Test
suite =
    describe "Desktop tests"
        [ describe "insert"
            [ test "Build a simple tree from Nothing" <|
                \_ ->
                    emptyDesktop
                        |> insert (newClient 17)
                        |> Expect.equal ((desktop << zipper << xx) (newClient 17))
            , test "Make a split in the first tree" <|
                \_ ->
                    desktop firstTree
                        |> insert (newClient 4)
                        |> Expect.equal (desktop secondTree)
            , test "Make a split in the second tree" <|
                \_ ->
                    desktop secondTree
                        |> insert (newClient 5)
                        |> Expect.equal (desktop thirdTree)
            ]
        , describe "layout"
            [ test "Do nothing when there's no client" <|
                \_ ->
                    emptyDesktop
                        |> layout
                        |> Expect.equal Cmd.none
            , test "Lay out a single client" <|
                \_ ->
                    let
                        id =
                            17

                        geometry =
                            { x = 0
                            , y = 0
                            , width = emptyDesktop.width
                            , height = emptyDesktop.height
                            }

                        command =
                            KWin.resize ( id, geometry )
                    in
                    emptyDesktop
                        |> insert (newClient id)
                        |> layout
                        |> Expect.equal (Cmd.batch [ command ])
            , test "Lay out firstTree" <|
                \_ ->
                    let
                        geometry1 =
                            { x = 0
                            , y = 0
                            , width = 500
                            , height = 1000
                            }

                        geometry2 =
                            { x = 500
                            , y = 0
                            , width = 500
                            , height = 400
                            }

                        geometry3 =
                            { x = 500
                            , y = 400
                            , width = 500
                            , height = 600
                            }

                        commands =
                            [ KWin.resize ( 1, geometry1 )
                            , KWin.resize ( 2, geometry2 )
                            , KWin.resize ( 3, geometry3 )
                            ]
                    in
                    desktop firstTree
                        |> layout
                        |> Expect.equal (Cmd.batch commands)
            , test "Lay out secondTree" <|
                \_ ->
                    let
                        geometry1 =
                            { x = 0
                            , y = 0
                            , width = 500
                            , height = 1000
                            }

                        geometry2 =
                            { x = 700
                            , y = 400
                            , width = 300
                            , height = 600
                            }

                        geometry3 =
                            { x = 500
                            , y = 400
                            , width = 200
                            , height = 600
                            }

                        geometry4 =
                            { x = 500
                            , y = 0
                            , width = 500
                            , height = 400
                            }

                        commands =
                            [ KWin.resize ( 1, geometry1 )
                            , KWin.resize ( 4, geometry4 )
                            , KWin.resize ( 3, geometry3 )
                            , KWin.resize ( 2, geometry2 )
                            ]
                    in
                    desktop secondTree
                        |> layout
                        |> Expect.equal (Cmd.batch commands)
            , test "Ignore minimized" <|
                \_ ->
                    let
                        geometry1 =
                            { x = 0
                            , y = 0
                            , width = 500
                            , height = 1000
                            }

                        geometry2 =
                            { x = 700
                            , y = 0
                            , width = 300
                            , height = 1000
                            }

                        geometry3 =
                            { x = 500
                            , y = 0
                            , width = 200
                            , height = 1000
                            }

                        commands =
                            [ KWin.resize ( 1, geometry1 )
                            , KWin.resize ( 3, geometry3 )
                            , KWin.resize ( 2, geometry2 )
                            ]

                        minimize tree =
                            case tree of
                                Leaf client ->
                                    Leaf { client | minimized = True }

                                _ ->
                                    tree
                    in
                    secondTree
                        |> modifySubtree minimize
                        |> desktop
                        |> layout
                        |> Expect.equal (Cmd.batch commands)
            ]
        ]
