module TestTree exposing (suite)

import Expect exposing (Expectation)
import Maybe exposing (andThen, map)
import Test exposing (..)
import Tree exposing (..)


fork : ( Tree a b, Tree a b ) -> a -> Tree a b
fork ( left, right ) root =
    Node root left right


tree =
    1
        |> fork
            ( 11
                |> fork ( Leaf 111, Leaf 112 )
            , 12
                |> fork
                    ( 121
                        |> fork ( Leaf 1211, Leaf 1212 )
                    , Leaf 122
                    )
            )


zipper : Zipper number number
zipper =
    zip tree


thenEnsure : (a -> Expectation) -> String -> Maybe a -> Expectation
thenEnsure expecting onFail m =
    case m of
        Nothing ->
            Expect.fail onFail

        Just y ->
            expecting y


thenCompareNode : a -> String -> Maybe (Zipper a b) -> Expectation
thenCompareNode value onFail =
    thenEnsure (Expect.equal (Just value) << getNode) onFail


thenCompareLeaf : b -> String -> Maybe (Zipper a b) -> Expectation
thenCompareLeaf value onFail =
    thenEnsure (Expect.equal (Just value) << getLeaf) onFail


suite : Test
suite =
    describe "Module Tree tests"
        [ describe "Tree.goToLeftChild"
            [ test "applied once should go to the node 11" <|
                \_ ->
                    zipper
                        |> goToLeftChild
                        |> thenCompareNode 11 "Couldn't reach child"
            , test "applied twice should go to the node 111" <|
                \_ ->
                    zipper
                        |> goToLeftChild
                        |> andThen goToLeftChild
                        |> thenCompareLeaf 111 "Couldn't reach child"
            , test "applied three times should find Nothing" <|
                \_ ->
                    zipper
                        |> goToLeftChild
                        |> andThen goToLeftChild
                        |> andThen goToLeftChild
                        |> Expect.equal Nothing
            ]
        , describe "Tree.goToRightChild"
            [ test "applied once should go to the node 12" <|
                \_ ->
                    zipper
                        |> goToRightChild
                        |> thenCompareNode 12 "Couldn't reach child"
            , test "applied twice should go to the node 122" <|
                \_ ->
                    zipper
                        |> goToRightChild
                        |> andThen goToRightChild
                        |> thenCompareLeaf 122 "Couldn't reach child"
            , test "applied three times should find Nothing" <|
                \_ ->
                    zipper
                        |> goToRightChild
                        |> andThen goToRightChild
                        |> andThen goToRightChild
                        |> Expect.equal Nothing
            ]
        , let
            down =
                zipper
                    |> goToRightChild
                    |> andThen goToLeftChild
                    |> andThen goToRightChild
          in
          describe "Tree.goUp"
            [ test "applied once should go to the node 121" <|
                \_ ->
                    down
                        |> andThen goUp
                        |> thenCompareNode 121 "Couldn't reach upper level"
            , test "applied twice should go to the node 12" <|
                \_ ->
                    down
                        |> andThen goUp
                        |> andThen goUp
                        |> thenCompareNode 12 "Couldn't reach upper level"
            , test "applied three times should go to the node 1" <|
                \_ ->
                    down
                        |> andThen goUp
                        |> andThen goUp
                        |> andThen goUp
                        |> thenCompareNode 1 "Couldn't reach upper level"
            , test "applied four times should find Nothing" <|
                \_ ->
                    down
                        |> andThen goUp
                        |> andThen goUp
                        |> andThen goUp
                        |> andThen goUp
                        |> Expect.equal Nothing
            , test "should build the right subtree when coming from right" <|
                \_ ->
                    zipper
                        |> goToRightChild
                        |> andThen goToLeftChild
                        |> andThen goToRightChild
                        |> andThen goUp
                        |> map subtree
                        |> Expect.equal (Just (121 |> fork ( Leaf 1211, Leaf 1212 )))
            , test "should build the right subtree when coming from left" <|
                \_ ->
                    zipper
                        |> goToRightChild
                        |> andThen goToLeftChild
                        |> andThen goToLeftChild
                        |> andThen goUp
                        |> map subtree
                        |> Expect.equal (Just (121 |> fork ( Leaf 1211, Leaf 1212 )))
            ]
        , let
            thenTest =
                thenEnsure
                    (goToRoot >> getNode >> Expect.equal (Just 1))
                    "Couldn't reach root"
          in
          describe "Tree.goToRoot"
            [ test "should go up one level properly" <|
                \_ -> zipper |> goToRightChild |> thenTest
            , test "should go up two levels properly" <|
                \_ ->
                    zipper
                        |> goToRightChild
                        |> andThen goToLeftChild
                        |> thenTest
            , test "should go up three levels properly" <|
                \_ ->
                    zipper
                        |> goToRightChild
                        |> andThen goToLeftChild
                        |> andThen goToLeftChild
                        |> thenTest
            , test "should stay at root" <|
                \_ ->
                    zipper
                        |> goToRoot
                        |> getNode
                        |> Expect.equal (Just 1)
            ]
        , describe "Tree.isLeftChild"
            [ test "should recognise a left child" <|
                \_ ->
                    zipper
                        |> goToRightChild
                        |> andThen goToLeftChild
                        |> Maybe.map isLeftChild
                        |> Expect.equal (Just True)
            , test "should recognise a single-element tree doesn't have a left child" <|
                \_ ->
                    zip (Leaf 1)
                        |> isLeftChild
                        |> Expect.false "Recognised a single-element tree as a left child"
            , test "shouldn't recognise a root node as a left child" <|
                \_ ->
                    zipper
                        |> isLeftChild
                        |> Expect.false "Recognised a single-element tree as a left child"
            ]
        , describe "Tree.isRightChild"
            [ test "should recognise a right child" <|
                \_ ->
                    zipper
                        |> goToLeftChild
                        |> andThen goToRightChild
                        |> Maybe.map isRightChild
                        |> Expect.equal (Just True)
            , test "should recognise a single-element tree doesn't have a right child" <|
                \_ ->
                    zip (Leaf 1)
                        |> isRightChild
                        |> Expect.false "Recognised a single-element tree as a right child"
            , test "shouldn't recognise a root node as a right child" <|
                \_ ->
                    zipper
                        |> isRightChild
                        |> Expect.false "Recognised a single-element tree as a right child"
            ]
        , describe "Tree.filter"
            [ test "should remove the leaf 1212" <|
                \_ ->
                    filter ((/=) 1212) tree
                        |> Expect.equal
                            (Just
                                (1
                                    |> fork
                                        ( 11 |> fork ( Leaf 111, Leaf 112 )
                                        , 12 |> fork ( Leaf 1211, Leaf 122 )
                                        )
                                )
                            )
            , test "should remove the odd leaves" <|
                \_ ->
                    filter (\n -> remainderBy 2 n == 0) tree
                        |> Expect.equal
                            (Just
                                (1
                                    |> fork
                                        ( Leaf 112
                                        , 12 |> fork ( Leaf 1212, Leaf 122 )
                                        )
                                )
                            )
            ]
        ]
